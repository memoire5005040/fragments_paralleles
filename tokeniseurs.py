!pip install spaCy
!python -m spacy download fr_core_news_sm

import os
import re
from alsatian_tokeniser import RegExpTokeniser    # https://zenodo.org/records/2454993
from nltk.tokenize import sent_tokenize
import nltk
nltk.download('punkt')
from nltk.tokenize import sent_tokenize
import spacy 
import fr_core_news_sm

def get_sentences_ALS(doc_ALS):
    """
    Tokeniseur pour l'alsacien (v. https://zenodo.org/record/2454993)
    """
    tokeniser = RegExpTokeniser()
    tokenised = tokeniser.tokenise(doc_ALS)
    sentences = [str(sentence) for sentence in tokenised.get_sentences()]
    sentences = [sentence for sentence in sentences if len(sentence)>0]
    return sentences

def get_sentences_DE(text, lang='german'):
    """
    Tokeniseur NLTK pour l'allemand 
    """
    return [s for p in text.split('\n') for s in sent_tokenize(p, language=lang)]

def get_sentences_FR(text, lang='french'):
    """
    Tokeniseur NLTK pour le français  
    """
    return [s for p in text.split('\n') for s in sent_tokenize(p, language=lang)]

def get_sentences_EN(text, lang="english"):
    """
    Tokeniseur NLTK pour l'anglais  
    """
    return [s for p in text.split('\n') for s in sent_tokenize(p, language=lang)]   

def get_sentences_ES(text, lang="spanish"):
    """
    Tokeniseur NLTK pour l'espagnol  
    """
    return [s for p in text.split('\n') for s in sent_tokenize(p, language=lang)]

def get_sentences_LB(text, lang="german"):
    """
    Tokeniseur NLTK pour le luxembourgeois  
    """
    return [s for p in text.split('\n') for s in sent_tokenize(p, language=lang)]

def get_sentences_OC(text, lang="spanish"):
    """
    Tokeniseur NLTK pour l'occitan  
    """
    return [s for p in text.split('\n') for s in sent_tokenize(p, language=lang)]

if __name__ == '__main__': 
    
    for article in os.listdir('strasbourgeois'):
        
        with open('strasbourgeois/'+article,'r',encoding='utf8') as f:
            doc_STRAS = f.read()
            phrases_STRAS = get_sentences_ALS(doc_STRAS)
            phrases_STRAS = """"""
    
        for i, phrase in enumerate(phrases_STRAS):
            phrases_STRAS += str(i) + '\t' + phrase + '\n'
        
        with open('strasbourgeois/'+article[:-4]+'_phrases.csv','w',encoding='utf8') as f:
            f.writelines(phrases_STRAS)
            
    for article in os.listdir('mulhousien'):
        
        with open('mulhousien/'+article,'r',encoding='utf8') as f:
            doc_MUL = f.read()
            sentences_MUL = get_sentences_ALS(doc_MUL)
            phrases_MUL = """"""
    
        for i, sentence in enumerate(sentences_MUL):
            phrases_MUL += str(i) + '\t' + sentence + '\n'
        
        with open('mulhousien/'+article[:-4]+'_phrases.csv','w',encoding='utf8') as f:
            f.writelines(phrases_MUL)
    
    for article in os.listdir('alsacien'):
        with open('alsacien/'+article,'r',encoding='utf8') as f:
            doc_ALS = f.read()
            sentences_ALS = get_sentences_ALS(doc_ALS)
            phrases_ALS = """"""
    
        for i, sentence in enumerate(sentences_ALS):
            phrases_ALS += str(i) + '\t' + sentence + '\n'
        
        with open('alsacien/'+article[:-4]+'_phrases.csv','w',encoding='utf8') as f:
            f.writelines(phrases_ALS)
            
    for article in os.listdir('occitan'):
        with open('occitan/'+article,'r',encoding='utf8') as f:
            doc_OC = f.read()
            sentences_OC = get_sentences_OC(doc_OC)
            phrases_OC = """"""
        for i, sentence in enumerate(sentences_OC):
            phrases_OC += str(i) + '\t' + sentence + '\n'
        
        with open('occitan/'+article[:-4]+'_phrases.csv','w',encoding='utf8') as f:
            f.writelines(phrases_OC)
    
    tokeniseur = spacy.load('fr_core_news_sm')
    
    for article in os.listdir('francais')[0:1]:
        
        with open('francais/'+article,'r',encoding='utf8') as f:
            doc_FR = f.read()
        
        sentences_FR = tokeniseur(doc_FR).sents
        
        phrases_FR = """"""
        
        for i, sentence in enumerate(sentences_FR):
            if not sentence.text.isspace():
                phrases_FR += str(i) + '\t' + sentence.text + '\n'
        
        with open('francais/'+article[:-4]+'_phrases.csv','w',encoding='utf8') as f: 
            f.writelines(phrases_FR)
            
            
    for article in os.listdir('allemand'):
        with open('allemand/'+article,'r',encoding='utf8') as f:
            doc_DE = f.read()
            sentences_DE = get_sentences_DE(doc_DE)
            phrases_DE = """"""
    
        for i, sentence in enumerate(sentences_DE):
            phrases_DE += str(i) + '\t' + sentence + '\n'
        
        with open('allemand/'+article[:-4]+'_phrases.csv','w',encoding='utf8') as f:
            f.writelines(phrases_DE)
    
    for article in os.listdir('anglais'):
        with open('anglais/'+article,'r',encoding='utf8') as f:
            doc_EN = f.read()
            sentences_EN = get_sentences_EN(doc_EN)
        phrases_EN = """"""
    
        for i, sentence in enumerate(sentences_EN):
            phrases_EN += str(i) + '\t' + sentence + '\n'
        
        with open('anglais/'+article[:-4]+'_phrases.csv','w',encoding='utf8') as f:
            f.writelines(phrases_EN)
            
    for article in os.listdir('espagnol'):
        with open('espagnol/'+article,'r',encoding='utf8') as f:
            doc_ES = f.read()
            sentences_ES = get_sentences_ES(doc_ES)
            phrases_ES = """"""
        for i, sentence in enumerate(sentences_ES):
            phrases_ES += str(i) + '\t' + sentence + '\n'
        
        with open('espagnol/'+article[:-4]+'_phrases.csv','w',encoding='utf8') as f:
            f.writelines(phrases_ES)
            
    for article in os.listdir('luxembourgeois'):
        with open('luxembourgeois/'+article,'r',encoding='utf8') as f:
            doc_LB = f.read()
            sentences_LB = get_sentences_LB(doc_LB)
            phrases_LB = """"""
        for i, sentence in enumerate(sentences_LB):
            phrases_LB += str(i) + '\t' + sentence + '\n'
        
        with open('luxembourgeois/'+article[:-4]+'_phrases.csv','w',encoding='utf8') as f:
            f.writelines(phrases_LB)
        