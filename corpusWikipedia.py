! pip install wikipedia-api

import requests 
from bs4 import BeautifulSoup
import wikipediaapi
import re

#liens vers les pages regroupant tous les articles dans chacune des variantes de l'alsacien 

liens_alsacien = ['https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pageuntil=Bischweiler#mw-pages', \
                 'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pagefrom=Bischweiler#mw-pages', \
                 'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pagefrom=Durrenbachunterelsass%0AD%C3%BCrrenbach+%28Unterelsass%29#mw-pages', \
                 'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pagefrom=Goxweiler#mw-pages', \
                 'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pagefrom=Kalhausen#mw-pages', \
                 'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pagefrom=Lipsheim#mw-pages', \
                 'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pagefrom=Niederenzen#mw-pages', \
                 'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pagefrom=Remelfangen#mw-pages', \
                 'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pagefrom=Siegen+%28Unterelsass%29#mw-pages', \
                 'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Elsassisch&pagefrom=Weiler+%28Unterelsass%29#mw-pages']

liens_mulhousien = ['https://als.wikipedia.org/wiki/Kategorie:Artikel_uf_Milh%C3%BCserisch', \
                   'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Milh%C3%BCserisch&pagefrom=Jean-Georges+Mieg#mw-pages', \
                   'https://als.wikipedia.org/w/index.php?title=Kategorie:Artikel_uf_Milh%C3%BCserisch&pagefrom=Schillinger%2C+Patricia%0APatricia+Schillinger#mw-pages']

liens_strasbourgeois = ['https://als.wikipedia.org/wiki/Kategorie:Artikel_uf_Strossburjerditsch']

def get_articles(lien):
    """Prend en paramètre le lien d'une catégorie de Wikipédia
    Retourne les noms des articles qu'elle contient
    Il faut avoir préalablement installé Requests et BeautifulSoup 
    """
    page = requests.get(lien).text
    soup = BeautifulSoup(page,'html.parser')
    liens = soup.select('div.mw-category-group ul li a')
    articles = [i.text for i in liens if 'Artikel uf' not in i.text]
    return articles

def page_als_texte(nom_article):
    """Prend en paramètre le nom d'un article de Wikipédia en alémanique 
    Retourne le texte de l'article
    Suppression de la bibliographie et des liens à la fin de la page pour éviter le texte en d'autres langues"""
    
    texte = wiki_als.page(nom_article).text 
    biblio = re.compile('L[i|ì]ter[a|à]t[u|ü]{1,2}r|Weblink\n')
    debut_biblio = re.search(biblio, texte)
    if debut_biblio != None:
        texte = texte[:debut_biblio.start()]   
    return texte 

def page_als(nom_article):
    """Prend en paramètre le nom d'une page de Wikipédia en alémanique 
    Retourne la page demandée comme objet"""
    page = wiki_als.page(nom_article)
    return page

def get_translation_fr(nom_article):
    """Prend en paramètre le nom de la traduction en français  
    Retourne le texte français sans la bibliographie
    """
    texte = wiki_fr.page(nom_article).text
    x = texte.find('Bibliographie\n')
    if x != -1:
        texte = texte[:x]
    return texte
    
def get_translation_co(nom_article):
    """Prend en paramètre le nom de la traduction en corse  
    Retourne le texte corse sans la bibliographie
    """
    texte = wiki_co.page(nom_article).text
    x = texte.find('Noti\n')
    if x != -1:
        texte = texte[:x]
    y = texte.find('Note\n')
    if y != -1:
        texte = texte[:y]
    return texte

def get_translation_oc(nom_article):
    """Prend en paramètre le nom de la traduction en occitan  
    Retourne le texte occitan sans la bibliographie
    """
    texte = wiki_oc.page(nom_article).text
    x = texte.find('Nòtas\n')
    if x != -1:
        texte = texte[:x]
    y = texte.find('Bibliografia\n')
    if y != -1:
        texte = texte[:y]
    z = texte.find('Nòtas e referéncias\n')
    if z != -1: 
        texte = texte[:z]
    return texte

def get_translation_de(nom_article):
    """Prend en paramètre le nom de la traduction en allemand  
    Retourne le texte allemand sans la bibliographie
    """
    texte = wiki_de.page(nom_article).text
    x = texte.find('Literatur\n')
    if x != -1:
        texte = texte[:x]
    y = texte.find('Weblinks\n')
    if y != -1:
        texte = texte[:y]
    return texte
    
def get_translation_it(nom_article):
    """Prend en paramètre le nom de la traduction en italien  
    Retourne le texte italien sans la bibliographie
    """
    texte = wiki_it.page(nom_article).text
    x = texte.find('Note\n')
    if x != -1:
        texte = texte[:x]
    y = texte.find('Bibliografia\n')
    if y != -1:
        texte = texte[:y]
    return texte
    
def get_translation_es(nom_article):
    """Prend en paramètre le nom de la traduction en espagnol  
    Retourne le texte espagnol sans la bibliographie
    """
    texte = wiki_es.page(nom_article).text
    x = texte.find('Bibliografía\n')
    if x != -1:
        texte = texte[:x]
    y = texte.find('Referencias\n')
    if y != -1:
        texte = texte[:y]
    return texte

def get_translation_en(nom_article):
    """Prend en paramètre le nom de la traduction en anglais  
    Retourne le texte anglais sans la bibliographie
    """
    texte = wiki_en.page(nom_article).text
    x = texte.find('See also\n')
    if x != -1:
        texte = texte[:x]
    y = texte.find('References\n')
    if y != -1:
        texte = texte[:y]
    return texte

def get_translation_lb(nom_article):
    """Prend en paramètre le nom de la traduction en luxembourgeois   
    Retourne le texte luxembourgeois sans la bibliographie
    """
    texte = wiki_lb.page(nom_article).text
    x = texte.find('Um Spaweck\n')
    if x != -1:
        texte = texte[:x]
    y = texte.find('Referenzen\n')
    if y != -1:
        texte = texte[:y]
    z = texte.find('Literatur\n')
    if z != -1:
        texte = texte[:z]
    return texte

if __name__ == '__main__':
    
    #sauvegarde des noms des articles dans des listes 
        
    articles_alsacien = []
    articles_mulhousien = []
    articles_strasbourgeois =[]
    
    for i in liens_alsacien:
        liste_articles = get_articles(i)
        for article in liste_articles:
            articles_alsacien.append(article) 
            
    for i in liens_mulhousien:
        liste_articles = get_articles(i)
        for article in liste_articles:
            articles_mulhousien.append(article)
    
    for i in liens_strasbourgeois:
        liste_articles = get_articles(i)
        for article in liste_articles:
            articles_strasbourgeois.append(article)
            
    wiki_als = wikipediaapi.Wikipedia('projet de recherche (adresse_mail)','als')
    wiki_oc = wikipediaapi.Wikipedia('projet de recherche (adresse_mail)','oc')
    wiki_co = wikipediaapi.Wikipedia('projet de recherche (adresse_mail)','co')
    wiki_fr = wikipediaapi.Wikipedia('projet de recherche (adresse_mail)','fr')
    wiki_de = wikipediaapi.Wikipedia('projet de recherche (adresse_mail)','de')
    wiki_it = wikipediaapi.Wikipedia('projet de recherche (adresse_mail)','it')
    wiki_es = wikipediaapi.Wikipedia('projet de recherche (adresse_mail)','es')
    wiki_en = wikipediaapi.Wikipedia('projet de recherche (adresse_mail)','en')
    wiki_lb = wikipediaapi.Wikipedia('projet de recherche (adresse_mail)','lb')
    
    char_interdits = r'[<>:\/?"]'
    regex = re.compile(char_interdits)
    
    for article in articles_strasbourgeois:
        with open('strasbourgeois/'+article+'.txt','w',encoding='utf8') as f: 
            f.write(page_als_texte(article))
    
    for article in articles_mulhousien:
        with open('mulhousien/'+article+'.txt','w',encoding='utf8') as f: 
            f.write(page_als_texte(article))
    
    for article in articles_alsacien:
        
        # Il y a 5 articles en alsacien dont les titres contiennent des caractères qui ne peuvent pas être utilisés 
        # dans les noms des fichiers .txt 
        # Ces articles ne sont pas sauvegardés  
        
        match = regex.search(article)
    
        if match:
            continue
        else:
            with open('alsacien/'+article+'.txt','w',encoding='utf8') as f: 
                f.write(page_als_texte(article)) 
                
    articles_als = articles_alsacien + articles_mulhousien + articles_strasbourgeois 
    
    #initialisation de dictionnaires pour stocker les noms des articles en 2 langues
    
    dico_ALS_FR = {}
    dico_ALS_DE = {}
    dico_ALS_EN = {}
    dico_ALS_LB = {}
    dico_OC_ES = {}
    dico_CO_IT = {}
    
    for article in articles_als:
    langlinks = page_als(article).langlinks 
    
    #Les articles sont stockés dans des fichiers .txt 
    #On a préalablement créé un dossier pour chaque langue 
    
    if 'fr' in langlinks.keys():
        titre_fr = langlinks['fr'].title
        texte_fr = get_translation_fr(titre_fr)
        with open('francais/'+titre_fr+'.txt','w',encoding='utf8') as f:
            f.write(texte_fr)
        dico_ALS_FR[article] = titre_fr
        
    if 'de' in langlinks.keys():
        titre_de = langlinks['de'].title
        match = regex.search(titre_de)
        if match:
            continue
        else:
            texte_de = get_translation_de(titre_de)
            with open('allemand/'+titre_de+'.txt','w',encoding='utf8') as f:
                f.write(texte_de) 
            dico_ALS_DE[article] = titre_de


    if 'lb' in langlinks.keys():
        titre_lb = langlinks['lb'].title
        match = regex.search(titre_lb)
        if match:
            continue
        else:
            texte_lb = get_translation_lb(titre_lb)
            with open('luxembourgeois/'+titre_lb+'.txt','w',encoding='utf8') as f:
                f.write(texte_lb) 
            dico_ALS_LB[article] = titre_lb

    if 'en' in langlinks.keys():
        titre_en = langlinks['en'].title
        match = regex.search(titre_en)
        if match:
            continue
        else:
            texte_en = get_translation_en(titre_en)
            with open('anglais/'+titre_en+'.txt','w',encoding='utf8') as f:
                f.write(texte_en) 
            dico_ALS_EN[article] = titre_en
    
    if 'oc' in langlinks.keys():
        titre_oc = langlinks['oc'].title
        texte_oc = get_translation_oc(titre_oc)
        with open('occitan/'+titre_oc+'.txt','w',encoding='utf8') as f:
            f.write(texte_oc)
        
        if 'es' in langlinks.keys():
            titre_es = langlinks['es'].title
            texte_es = get_translation_es(titre_es)
            with open('espagnol/'+titre_es+'.txt','w',encoding='utf8') as f:
                f.write(texte_es)
            dico_OC_ES[titre_oc] = titre_es
        
    if 'co' in langlinks.keys():
        titre_co = langlinks['co'].title
        texte_co = get_translation_co(titre_co)
        with open('corse/'+titre_co+'.txt','w',encoding='utf8') as f:
            f.write(texte_co)
        
        if 'it' in langlinks.keys():
            titre_it = langlinks['it'].title
            texte_it = get_translation_it(titre_it)
            with open('italien/'+titre_it+'.txt','w',encoding='utf8') as f:
                f.write(texte_it)
            dico_CO_IT[titre_co] = titre_it
            
    #Les titres des traductions sont stockés dans des fichiers .txt 
    #un fichier pour chaque paire de langues  
    
    with open('dico_ALS_FR.txt','w',encoding='utf8') as f:
        for als, fr in dico_ALS_FR.items():
            f.write(als + '\t' + fr + '\n')
    
    with open('dico_ALS_DE.txt', 'w', encoding='utf8') as f:
        for als, de in dico_ALS_DE.items():
            f.write(als + '\t' + de + '\n')
            
    with open('dico_ALS_LB.txt','w',encoding='utf8') as f: 
        for als, lb in dico_ALS_LB.items():
            f.write(als + '\t' + lb + '\n')
            
    with open('dico_ALS_EN.txt','w',encoding='utf8') as f: 
        for als, en in dico_ALS_EN.items():
            f.write(als + '\t' + en + '\n')
            
    with open('dico_OC_ES.txt','w',encoding='utf8') as f:
        for oc, es in dico_OC_ES.items():
            f.write(oc + '\t' + es + '\n')
    
    with open('dico_CO_IT.txt', 'w', encoding='utf8') as f:
        for co, it in dico_CO_IT.items():
            f.write(co + '\t' + it + '\n')
            
