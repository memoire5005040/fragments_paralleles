! pip install sentence-transformers
! pip install faiss-cpu
! pip install datasets

from datasets import Dataset
from sentence_transformers import SentenceTransformer
from nltk.tokenize import sent_tokenize
import os
import random
import re
import pandas as pd

def get_random_sentences_OC(article_pair):
    """
    Prend en paramètre une liste contenant une paire d'articles en occitan/ langue B
    Retourne une liste de listes contenant : identifiant de la phrase, phrase, titre de l'article    
    """
    title = article_pair[0] + '_phrases.csv'    # un fichier [nom]_phrases.csv pour chaque article dans le dossier 'phrases'
    sentences_list = [] 
    if title in sent_oc:
        with open('occitan/phrases/'+title,'r',encoding='utf8') as f:
            for line in f:
                new_line = re.split('\t', line)
                new_line[1] = new_line[1].strip()
                new_line.append(title[:-12])
                sentences_list.append(new_line)
    
    # récupération de 10 phrases aléatoires en occitan 
    # si l'article en contient au moins 10
    
    if len(sentences_list)>10:
        random.seed(2)
        x = random.sample(sentences_list, k=10)
        return x
    elif len(sentences_list)<=10:    
        return sentences_list
    
def get_sentences_ES(article_pair):
    """
    Prend en paramètre une liste contenant une paire d'articles en langue A/ espagnol
    Retourne une liste de listes contenant : identifiant de la phrase, phrase, titre de l'article 
    """
    title = article_pair[1] + '_phrases.csv'
    sentences_list = []
    if title in sent_es:
        with open('espagnol/phrases/'+title,'r',encoding='utf8') as f:
            for line in f:
                new_line = re.split('\t', line)
                new_line[1] = new_line[1].strip()
                new_line.append(title[:-12])
                sentences_list.append(new_line) 
        return sentences_list  
    else:
        pass

def retrieve_search_df(sentence, top_k = 3):
    sentence_embedding = encoder.encode(sentence)
    # les scores correspondent à une mesure de distance : plus la distance est faible, plus les phrases sont proches
    scores, retrieved_examples = dataset.get_nearest_examples('embeddings', sentence_embedding, k=top_k)
    sent_df = pd.DataFrame(retrieved_examples)
    sent_df["score"] = scores
    return sent_df[['sentence', 'score']]

if __name__ == '__main__':
    
    # initialisation d'une liste pour stocker les paires d'articles 
    
    article_pairs = []
    
    # dico_OC_ES.txt : titre occitan \t titre espagnol
    # récupération de 15 paires d'articles aléatoires 
    
    with open('dico_OC_ES.txt','r',encoding='utf8') as f:
        lines = f.read().splitlines()
        random.seed(2)
        for i in range(15):
            article = random.choice(lines)
            article_pairs.append(re.split('\t',article))
    print(article_pairs)
        
    sent_oc = os.listdir('occitan/phrases')
    sent_es = os.listdir('espagnol/phrases')
    
    # https://huggingface.co/sentence-transformers/LaBSE  
    
    encoder = SentenceTransformer('sentence-transformers/LaBSE')
    annotation_doc = """"""
    
    for i in article_pairs:
        
        sent_OC = get_random_sentences_OC(i)
        sent_ES = get_sentences_ES(i)
        
        print(sent_OC)
        print('\n\n')
        
        sent_ES_pd = pd.DataFrame(((id, sent, article) for id, sent, article in sent_ES), columns = ['id','sentence','article'])
        
        sent_ES_pd['embeddings'] = sent_ES_pd['sentence'].apply(encoder.encode)
        
        # ajout d'un index FAISS pour faciliter la recherche sémantique
        # https://huggingface.co/learn/nlp-course/chapter5/6?fw=tf
        
        dataset = Dataset.from_pandas(sent_ES_pd)
        dataset.add_faiss_index(column='embeddings')
        
        for sentence_oc in sent_OC: 
            res = retrieve_search_df(sentence_oc[1])
            
            # création d'un fichier .csv avec des paires de phrases candidates à annoter 
            # phrase OC - article OC - indice OC - score - phrase ES - article ES - indice ES 
            # séparateur : \t 
            
            for i in range(len(res)):
                annotation_doc += (sentence_oc[1] + \
                '\t' + sentence_oc[2] + \
                '\t' + sentence_oc[0] + \
                '\t' + str(res.iloc[i].score) +\
                '\t' + \
                res.iloc[i].sentence \
                + '\t' \
                + sent_ES_pd[sent_ES_pd.sentence == res.iloc[i].sentence].article.values[0]+'\t'+ \
                sent_ES_pd[sent_ES_pd.sentence == res.iloc[i].sentence].id.values[0] + '\n')
        annotation_doc += '\n\n'

    with open('phrasesCandidates_OC_ES.csv','w',encoding='utf8') as f:
        f.writelines(annotation_doc)
      
